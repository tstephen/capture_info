import { Component, OnInit } from '@angular/core';
import { UserCreateComponent } from '../user-create/user-create.component';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
