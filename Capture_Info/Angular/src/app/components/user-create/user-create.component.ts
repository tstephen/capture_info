import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { User } from 'src/app/models/user';
import { formatDate } from '@angular/common';
import { Response } from '../../models/responseModel';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  firstName =  '';
  surname  =  '';
  id  =  '';
  dateOfBirth  = new  Date();
  serverResponse: Response;
  successMessage: string;
  errorMessage: string;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit() {
  }

  insertNewUser() {
    const newUser: User = {
      name: this.firstName,
      surname: this.surname,
      id: this.id,
      dateOfBirth: formatDate(this.dateOfBirth, 'yyMMdd', 'en')
    };

    this.api.createUser(newUser).subscribe(data => {
      this.serverResponse = JSON.parse(JSON.stringify(data));
      if (this.serverResponse.success === true) {
        this.successMessage = 'Successfully added user.';
        this.errorMessage = null;
        setTimeout(() => {
          this.router.navigate(['/display']);
        });
      } else {
        this.errorMessage = 'Unable to add user. Fix errors.';
        this.successMessage = null;
      }
    }, error => {
      this.errorMessage = 'Unable to contact backend service. Please try again later.';
    });
  }

}
