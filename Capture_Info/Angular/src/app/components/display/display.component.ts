import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { User } from 'src/app/models/user';
import { Response } from 'src/app/models/ResponseModel';
import { formatDate } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {

  columnsToDisplay = ['userFirstName', 'userSurname', 'userId', 'userDateOfBirth', 'edit', 'delete'];
  Users = [];
  UserLength: number;
  deleteUserMessage: string;
  deleteUserErrorMessage: string;
  userToEdit: User;
  editMode: boolean = false;
  errorMessage: string;
  successMessage: string;
  serverResponse: Response;
  convertedDate: Date;
  oldUser: User;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit() {
    this.api.getUsers().subscribe(data => {
      this.Users = data;
    });
    this.UserLength = this.Users.length;
  }

  editUser(user)
  {
    this.oldUser = user;
    this.editMode = true;
    this.userToEdit = new User();
    this.userToEdit.name = user.name;
    this.userToEdit.surname = user.surname;
    this.userToEdit.id = user.id;
    this.userToEdit.dateOfBirth = user.dateOfBirth;
  }

  deleteUser(user)
  {
    this.api.deleteUser(user).subscribe(data => {
      this.deleteUserMessage = 'Successfully deleted user.';
      setTimeout(() => {
        window.location.reload();
      }, 200);
    }, error => {
      this.deleteUserErrorMessage = 'Something went wrong. Try again later.';
    });
  }

  submitEditUser() {
    this.userToEdit.dateOfBirth = formatDate(this.convertedDate, 'yyMMdd', 'en');
    this.api.updateUser(this.userToEdit, this.oldUser).subscribe(data => {
      if (data['success']) {
        this.successMessage = 'Successfully updated information.';
        setTimeout(() => {
          this.editMode = false;
          this.router.navigate(['/insert']);
        }, 500);
      } else {
        this.errorMessage = 'Failed to update user.';
      }
    }, error => {
      this.errorMessage = 'Backend problem, try again later.';
    });
  }

  goBack() {
    this.editMode = false;
  }

}
