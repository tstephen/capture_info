import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { ApiService } from './services/api.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule, MatCardModule, MatInputModule,
         MatFormFieldModule, MatCheckboxModule, MatDatepickerModule,
         MatRadioModule, MatSelectModule } from '@angular/material';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { FormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Routes, RouterModule } from '@angular/router';
import { InsertComponent } from './components/insert/insert.component';
import { DisplayComponent } from './components/display/display.component';
import { MatTableModule } from '@angular/material/table';

const appRoutes: Routes = [
  { path: 'insert', component: InsertComponent },
  { path: 'display', component: DisplayComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UserCreateComponent,
    MainNavComponent,
    InsertComponent,
    DisplayComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    LayoutModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatRadioModule,
    MatSelectModule,
    MatNativeDateModule,
    MatToolbarModule,
    HttpClientModule,
    FormsModule,
    MatTableModule
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
