import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURL = 'http://localhost:57653/Api/UserInfo';

  constructor(private httpClient: HttpClient) { }

  public createUser(user: User) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.post(`${this.apiURL}/InsertNewUser`, user, httpOptions);
  }

  public updateUser(userToEdit: User, old_user: User) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.post(`${this.apiURL}/UpdateUser`, { newUserInfo: userToEdit, oldUser: old_user }, httpOptions);
  }

  public deleteUser(user: User) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.post(`${this.apiURL}/DeleteUser`, user, httpOptions);
  }

  public getUserByName(name: string) {}

  public getUsers() {
    return this.httpClient.get<User[]>(`${this.apiURL}/AllInfo`);
  }

}
