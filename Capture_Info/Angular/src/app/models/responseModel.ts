import { User } from './user';

export class Response {
  user: User;
  success: boolean;
  errors: string[];
}
