﻿
var submitButton = document.getElementById('submitButton');
submitButton.addEventListener('click', Validate);

function mit() {
	/*Put all the data posting code here*/
	document.getElementById("myForm").reset();
}

function Validate() {
	var name = document.getElementById("txtName");
	var surname = document.getElementById("txtSurname");
	var id = document.getElementById("txtID");
	var dateofbirth = document.getElementById("txtDate");
	if (name.value == "") {
		$(name).css('border', '2px solid red');
		return false;
	} else {
		$(name).css('border', '2px solid green');
	}

	if (surname.value == "") {
		$(surname).css('border', '2px solid red');
		return false;
	} else {
		$(surname).css('border', '2px solid green');
	}

	if (id.value == "") {
		id.focus();
		$(id).css('border', '2px solid red');
		return false;
	} else {
		$(id).css('border', '2px solid green');
	}

	if (dateofbirth.value == "") {
		dateofbirth.focus();
		$(dateofbirth).css('border', '2px solid red');
		return false;
	} else {
		$(dateofbirth).css('border', '2px solid green');
	}
	return true;
}

