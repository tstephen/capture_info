﻿using System.Web.Mvc;
using Capture_Info.Repository;
using Capture_Info.Model;
using Validation;
using Validation.Factories;

namespace WebCaptureInfo.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(UserInfoModel user)
        {
	        ViewBag.tempName = user.name;
	        ViewBag.tempSurname = user.surname;
	        ViewBag.tempId = user.id;
	        ViewBag.tempDateofbirth= user.dateOfBirth;

			IValidationFactory factory = new ValidationFactory();
            IRepository repo = new UserRepository();

            if(string.IsNullOrEmpty(user.name) || string.IsNullOrEmpty(user.surname)
                || string.IsNullOrEmpty(user.id) || string.IsNullOrEmpty(user.dateOfBirth))
            {
	            ViewBag.Errorform = "All fields are required.";
	            return View();
            }

			var result = factory.GetInstance(ValidationField.FirstName).Validate(user.name);

            if (!result.IsValid)
            {
	            ViewBag.ErrorMessageName = result.ValidationMessage;
	            ViewBag.ErrorName = true;
            }

            result = factory.GetInstance(ValidationField.Surname).Validate(user.surname);

            if (!result.IsValid)
            {
	            ViewBag.ErrorMessageSurname = result.ValidationMessage;
	            ViewBag.ErrorSurname = true;
            }

            result = factory.GetInstance(ValidationField.ID).Validate(user.id);

            if (!result.IsValid)
            {
	            ViewBag.ErrorMessageId = result.ValidationMessage;
	            ViewBag.ErrorId = true;
            }

            result = factory.GetInstance(ValidationField.DateOfBirth).Validate(user.dateOfBirth);

            if (!result.IsValid)
            {
	            ViewBag.ErrorMessageDateofbirth = result.ValidationMessage;
	            ViewBag.ErrorDateofbirth = true;
            }

            if (ViewBag.ErrorMessageName != null || ViewBag.ErrorMessageSurname != null
                || ViewBag.ErrorMessageId != null || ViewBag.ErrorMessageDateofbirth != null)
	            return View();

            repo.SaveToPath(user);
            ViewBag.Success = "Success, your info has been saved.";
            return View();
        }

        [HttpGet]
        public ActionResult DisplayText()
        {
            var repo = new UserRepository();
            var textResult = repo.GetFromPath();
            textResult = textResult.Replace("\n", "<br/>");
            ViewBag.TextResult = textResult;
            return View();
        }
    }
}