﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Rules
{
	public interface IValidationRule
	{
		ValidationResult Result { get; }
		bool IsValid(string s);
        bool IsValid();
        void SetInvalidResult(string message);
	}
}
