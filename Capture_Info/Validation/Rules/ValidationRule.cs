﻿using Validation.Rules;

namespace Validation
{
	public class ValidationRule : IValidationRule
	{
		public ValidationResult Result { get; private set; } = new ValidationResult("Success");
		public virtual bool IsValid()
		{
			return Result.IsValid;
		}

        public virtual bool IsValid(string s) 
        {
            return Result.IsValid;
        }

		public void SetInvalidResult( string message)
		{
			Result.IsValid = false;
			Result.ValidationMessage = message;
		}
	}
}