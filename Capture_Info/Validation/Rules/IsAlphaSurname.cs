﻿using System.Text.RegularExpressions;

namespace Validation
{
	public class IsAlphaSurname : ValidationRule
	{
		public override bool IsValid(string s)
		{
			var isMatch = new Regex(@"^([a-zA-Z]+( {1}|-{1})?[a-zA-Z])*$").IsMatch(s);
            if (!isMatch)
            {
                SetInvalidResult("No Special Character allowed.");
            }
            return Result.IsValid;
		}
	}
}
