﻿using System.Text.RegularExpressions;


namespace Validation
{
	public class IsAlphaName : ValidationRule
	{
		public override bool IsValid(string s)
        {
            bool isMatch = IsAlpha(s);
            if (!isMatch)
            {
                SetInvalidResult("No Special Character allowed.");
            }
            return Result.IsValid;
        }

        private static bool IsAlpha(string s)
        {
            return new Regex(@"^[a-zA-Z]+$").IsMatch(s);
        }
    }
}
