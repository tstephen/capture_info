﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Rules
{
	class IsCorrectLength : ValidationRule
	{
		private readonly int length;

		public IsCorrectLength(int length = -1)
		{
			this.length = length;
		}

		public override bool IsValid(string s)
		{
			if (length == -1 || s.Length == length)
				return Result.IsValid;

			SetInvalidResult("Incorrect length");
			return Result.IsValid;
		}
	}
}
