﻿namespace Validation
{
	public class MaxLength : ValidationRule
	{
		private const int _MAX_LENGTH = 30;

		public override bool IsValid(string s)
		{
			var isValid = s?.Length > _MAX_LENGTH ? false : true;
            if (!isValid)
            {
                SetInvalidResult("Length too long.");
            }
            return Result.IsValid;
		}
	}
}
