﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation
{
	public class IdLength : ValidationRule
	{
		public override bool IsValid(string s)
		{
			var isMatch = s.Length == 13 ? true : false;
            if (!isMatch)
            {
                SetInvalidResult("Length must be 13.");
            }
            return Result.IsValid;
		}
	}
}
