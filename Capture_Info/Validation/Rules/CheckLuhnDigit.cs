﻿using System;
using System.Collections.Generic;

namespace Validation
{
    public class CheckLuhnDigit : ValidationRule
    {
        public override bool IsValid(string s)
        {
            var digits = new List<int>();

            for (int i = 0; i < s.Length; i++)
            {
                digits.Add(Convert.ToInt32(s[i]) - 48);
            }

            for (int i = 0; i < digits.Count; i++)
            {
                if (i % 2 == 1)
                {
                    digits[i] *= 2;
                }
            }

            int sum = 0;
            for (int i = 0; i < digits.Count; i++)
            {
                if (digits[i] > 9)
                {
                    string groupedDigits = Convert.ToString(digits[i]);
                    if (groupedDigits.Length > 1)
                    {
                        digits[i] = Convert.ToInt32(groupedDigits[0] - 48) + Convert.ToInt32(groupedDigits[1] - 48);
                    }
                }
                if (i != digits.Count - 1)
                    sum += digits[i];
            }

            if ((sum * 9) % 10 != digits[digits.Count - 1])
            {
				 SetInvalidResult("Invalid checksum.");
			}

			return Result.IsValid;
        }
    }
}