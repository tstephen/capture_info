﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Validation
{
	public class OnlyDigitsInString : ValidationRule
	{
		public override bool IsValid(string s)
		{
			var isMatch = new Regex(@"^[0-9]+$").IsMatch(s);
            if (!isMatch)
            {
                SetInvalidResult("Only digits allowed.");
            }
            return Result.IsValid;
		}
	}
}
