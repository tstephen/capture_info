﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation
{
	public class IsValidResidencyCode : ValidationRule
	{
		public override bool IsValid(string s)
		{
			if (s[10] != '0' && s[10] != '1')
			{
                SetInvalidResult("Invalid residency code.");
			}
            return Result.IsValid;
		}
	}
}
