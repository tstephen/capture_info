﻿using System.Text.RegularExpressions;

namespace Validation
{
	public class IsNoDigitInString : ValidationRule
	{
		public override bool IsValid(string s)
		{
			var isMatch = new Regex(@"[0-9]+").IsMatch(s);
            if (isMatch)
            {
                SetInvalidResult("No digits allowed.");
            }
            return Result.IsValid;
		}
	
	}
}
