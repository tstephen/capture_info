﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation
{
	public class IsDateValid : ValidationRule
	{
		public override bool IsValid(string s)
		{
			try
			{
				s = s.Substring(0, 6);
			}
			catch (Exception e)
			{
				SetInvalidResult("Incorrect date format.");
				return Result.IsValid;
			}
			
			bool isMatch = DateTime.TryParseExact(s, "yyMMdd", null, DateTimeStyles.None, out DateTime dateValue);
            if (!isMatch)
            {
                SetInvalidResult("Date is invalid.");
            }
            return Result.IsValid;
		}
	}
}
