﻿namespace Validation
{
	public class ValidationResult
	{
		public bool IsValid { get; set; }
		public string ValidationMessage { get; set; }

		public ValidationResult(string defaultMessage = "")
		{
			IsValid = true;
			ValidationMessage = defaultMessage;
		}
    }
}