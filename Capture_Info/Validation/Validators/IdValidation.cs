﻿using Validation.Factories;
using Validation.Validators;

namespace Validation
{
    public class IdValidation : IValidation
    {
        private readonly IRulesBuilderFactory factory;
        public ValidationResult Result { get; private set; } = new ValidationResult("Success");

        public IdValidation(IRulesBuilderFactory rulesFactory)
        {
            factory = rulesFactory;
		}

        public ValidationResult Validate(string s)
        {
            var rules = factory.GetInstance(ValidationField.ID).Create();

            foreach (ValidationRule rule in rules)
            {
                if (!rule.IsValid(s))
                {
                    return rule.Result;
                }
            }
            return Result;
        }
    }
}
