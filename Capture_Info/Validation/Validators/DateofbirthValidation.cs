﻿using Validation.Factories;
using Validation.Validators;

namespace Validation
{
	public class DateofbirthValidation : IValidation
	{
		private readonly IRulesBuilderFactory factory;
		public ValidationResult Result { get; private set; } = new ValidationResult("Success");

		public DateofbirthValidation(IRulesBuilderFactory rulesFactory)
		{
			factory = rulesFactory;
		}

		public ValidationResult Validate(string birthDate)
		{
			var rules = factory.GetInstance(ValidationField.DateOfBirth).Create();

			foreach (ValidationRule rule in rules)
			{
				if (!rule.IsValid(birthDate))
				{
					return rule.Result;
				}
			}
			return Result;
		}
	}
}