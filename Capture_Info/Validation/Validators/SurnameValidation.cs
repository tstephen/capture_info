﻿using Validation.Factories;
using Validation.Validators;

namespace Validation
{
    public class SurnameValidation : IValidation
    {
		private readonly IRulesBuilderFactory factory;
        public ValidationResult Result { get; private set; } = new ValidationResult("Success");

        public SurnameValidation(IRulesBuilderFactory rulesFactory)
        {
            factory = rulesFactory;
		}
 
		public ValidationResult Validate(string s)
        {
            var rules = factory.GetInstance(ValidationField.Surname).Create();

            foreach (ValidationRule rule in rules)
            {
                if (!rule.IsValid(s))
                {
                    return rule.Result;
                }
            }
			return Result;
        }
    }
}
