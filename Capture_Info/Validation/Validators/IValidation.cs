﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Validators
{
	public interface IValidation
	{
        ValidationResult Result { get; }
        ValidationResult Validate(string field);	
	}
}
