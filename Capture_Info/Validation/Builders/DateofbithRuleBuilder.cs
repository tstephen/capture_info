﻿using System.Collections.Generic;
using Validation.Rules;

namespace Validation
{
	public class DateofbirthRulesBuilder : IRulesBuilder
	{
		private readonly Factories.IRulesFactory factory;

		public DateofbirthRulesBuilder(Factories.IRulesFactory f)
		{
			factory = f;
		}
	public List<IValidationRule> Create()
		{
			return factory.CreateRulesList(Factories.ValidationField.DateOfBirth);
		}
	}
}