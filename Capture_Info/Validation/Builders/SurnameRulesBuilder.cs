﻿using System.Collections.Generic;
using Validation.Rules;

namespace Validation
{
    public class SurnameRulesBuilder : IRulesBuilder
    {
        private readonly Factories.IRulesFactory factory;

        public SurnameRulesBuilder(Factories.IRulesFactory f)
        {
            factory = f;
        }

        public  List<IValidationRule> Create()
        {
			return factory.CreateRulesList(Factories.ValidationField.Surname);
		}
    }
}