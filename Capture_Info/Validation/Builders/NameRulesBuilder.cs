﻿using System.Collections.Generic;
using Validation.Rules;

namespace Validation
{
	public class NameRulesBuilder : IRulesBuilder
	{
        private readonly Factories.IRulesFactory factory;
        public NameRulesBuilder(Factories.IRulesFactory f)
        {
            factory = f;
        }

        public  List<IValidationRule> Create()
        {
			return factory.CreateRulesList(Factories.ValidationField.FirstName);
        }
    }
}