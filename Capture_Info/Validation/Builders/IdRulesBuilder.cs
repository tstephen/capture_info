﻿using System.Collections.Generic;
using Validation.Rules;

namespace Validation
{
    public class IdRulesBuilder : IRulesBuilder
    {
        private readonly Factories.IRulesFactory factory;

        public IdRulesBuilder(Factories.IRulesFactory f)
        {
            factory = f;
        }

        public  List<IValidationRule> Create()
        {
			return factory.CreateRulesList(Factories.ValidationField.ID);
		}
    }
}