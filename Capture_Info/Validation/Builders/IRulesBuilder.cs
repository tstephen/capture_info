﻿using System.Collections.Generic;
using Validation.Rules;

namespace Validation
{
	public interface IRulesBuilder
	{
         List<IValidationRule> Create();
	}
}