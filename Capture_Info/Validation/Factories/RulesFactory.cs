﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Rules;

namespace Validation.Factories
{
	public class RulesFactory : IRulesFactory
	{
		//List Of Rules For ValidationField
		public List<IValidationRule> CreateRulesList(ValidationField field)
		{
			switch (field)
			{
				case ValidationField.FirstName:
					return new List<IValidationRule>
					{
						new MaxLength(),
						new IsNoDigitInString(),
						new IsAlphaName()
					};
				case ValidationField.Surname:
					return new List<IValidationRule>
					{
						new IsNoDigitInString(),
						new IsAlphaSurname()
					};
				case ValidationField.ID:
					return new List<IValidationRule>
					{
						new OnlyDigitsInString(),
						new IdLength(),
						new IsDateValid(),
						new IsValidResidencyCode(),
						new CheckLuhnDigit()
					};
				case ValidationField.DateOfBirth:
					return new List<IValidationRule>
					{
						new IsCorrectLength(6),
						new IsDateValid()
					};
				default:
					return new List<IValidationRule>();
			}
		}
	}
}
