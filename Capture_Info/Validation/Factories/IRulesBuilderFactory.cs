﻿namespace Validation.Factories
{
	public interface IRulesBuilderFactory
	{
		IRulesBuilder GetInstance(ValidationField field);
	}
}
