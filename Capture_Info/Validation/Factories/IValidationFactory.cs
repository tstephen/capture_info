﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Validators;

namespace Validation.Factories
{
	public interface IValidationFactory
	{
		IValidation GetInstance(ValidationField field);
	}
}
