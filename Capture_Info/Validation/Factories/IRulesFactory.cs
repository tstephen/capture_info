﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Validation.Rules;

namespace Validation.Factories
{
	public enum ValidationField { FirstName, Surname, ID, DateOfBirth }
	public interface IRulesFactory
	{
		List<IValidationRule> CreateRulesList(ValidationField field);
	}
}
