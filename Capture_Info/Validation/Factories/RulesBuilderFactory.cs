﻿using Validation.Factories;

namespace Validation
{
	public class RulesBuilderFactory : IRulesBuilderFactory
	{
        IRulesFactory factory;

        public RulesBuilderFactory(IRulesFactory f)
        {
            factory = f;
        }

		public IRulesBuilder GetInstance(ValidationField field)
        {
            switch (field)
            {
                case ValidationField.FirstName:
					return new NameRulesBuilder(factory);
                case ValidationField.Surname:
					return  new SurnameRulesBuilder(factory); 
                case ValidationField.ID:
					return new IdRulesBuilder(factory);
				case ValidationField.DateOfBirth:
					return new DateofbirthRulesBuilder(factory);
				default:
                    return null;
            }
        }
	}
}