﻿using Validation.Factories;
using Validation.Validators;

namespace Validation
{
    public class ValidationFactory : IValidationFactory
    {
        IRulesBuilderFactory factory = new RulesBuilderFactory(new RulesFactory());

        public IValidation GetInstance(ValidationField field)
        {
            switch (field)
            {
                case ValidationField.FirstName:
					return new NameValidation(factory);
				case ValidationField.Surname:
					return new SurnameValidation(factory);
				case ValidationField.ID:
					return new IdValidation(factory);
				case ValidationField.DateOfBirth:
					return new DateofbirthValidation(factory);
				default:
                    return null;
            }
        }

    }
}
