﻿namespace Capture_Info.Model
{
    public class UserInfoModel
    {

		public string name { get; set; }
        public string surname { get; set; }
        public string id { get; set; }
		public string dateOfBirth { get; set; }

		public UserInfoModel()
        {
            name = "";
            surname = "";
            id = "";
			dateOfBirth = "";

		}

        public override string ToString()
        {
            return $"Name: {name}, Surname: {surname}, ID: {id}, Dateofbirth: {dateOfBirth}";
        }
	}
}
