﻿using Capture_Info.Model;
using System.Collections.Generic;

namespace Capture_Info.Repository
{
    public interface IRepository
    {
        void SaveToPath(UserInfoModel user);
        string GetFromPath();
        List<UserInfoModel> GetInfoByFirstName(string firstName);
        List<UserInfoModel> GetAllInfo();
        void SaveUser(UserInfoModel user);
        void UpdateUser(UserInfoModel user, UserInfoModel oldUser);
        void DeleteUser(UserInfoModel user);
    }
}
