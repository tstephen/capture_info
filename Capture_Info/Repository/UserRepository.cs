﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using Capture_Info.Model;

namespace Capture_Info.Repository
{
    public class UserRepository : IRepository
    {
        private string _path = HostingEnvironment.MapPath("~/App_Data/Information.txt");

        public void SaveToPath(UserInfoModel user)
        {
            if (!File.Exists(_path))
            {
                using (StreamWriter sw = File.CreateText(_path))
                {
                    sw.WriteLine($"Name: {user.name}, Surname: {user.surname}, ID: {user.id}, Dateofbirth: {user.dateOfBirth}");
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(_path))
                {
					sw.WriteLine($"Name: {user.name}, Surname: {user.surname}, ID: {user.id}, Dateofbirth: { user.dateOfBirth}");
                }
            }
        }

        public string GetFromPath()
        {
            try
            {
                using (StreamReader sr = File.OpenText(_path))
                {
                    string s = "";
                    string totalString = "";

                    while ((s = sr.ReadLine()) != null)
                    {
                        totalString += s + '\n';
                    }
                    return totalString;
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }            
        }

        public List<UserInfoModel> GetInfoByFirstName(string firstName)
        {          
            var users = new List<UserInfoModel>();

            try
            {
                using (StreamReader sr = File.OpenText(_path))
                {
                    var s = "";

                    while ((s = sr.ReadLine()) != null)
                    {
                        var userInfo = s.Split(',');
                        var user = new UserInfoModel();

                        user.name = userInfo[0].Substring(userInfo[0].Trim().IndexOf(' ') + 1);
                        user.surname = userInfo[1].Substring(userInfo[1].Trim().IndexOf(' ') + 1);
                        user.id = userInfo[2].Substring(userInfo[2].Trim().IndexOf(' ') + 1);
                        user.dateOfBirth = userInfo[3].Substring(userInfo[3].Trim().IndexOf(' ') + 1);

                        if (user.name.CompareTo(firstName) == 0)
                        {
                            users.Add(user);
                        }
                    }
                }
                return users;
            }
            catch
            {
                throw;
            }
        }

        public List<UserInfoModel> GetAllInfo()
        {
            var users = new List<UserInfoModel>();

            try
            {
                using (StreamReader sr = File.OpenText(_path))
                {
                    var s = "";

                    while ((s = sr.ReadLine()) != null)
                    {
                        var userInfo = s.Split(',');
                        var user = new UserInfoModel();

                        userInfo[0] = userInfo[0].Trim();
                        userInfo[1] = userInfo[1].Trim();
                        userInfo[2] = userInfo[2].Trim();
                        userInfo[3] = userInfo[3].Trim();

                        user.name = userInfo[0].Substring(userInfo[0].IndexOf(' ') + 1);
                        user.surname = userInfo[1].Substring(userInfo[1].IndexOf(' ') + 1);
                        user.id = userInfo[2].Substring(userInfo[2].IndexOf(' ') + 1);
                        user.dateOfBirth = userInfo[3].Substring(userInfo[3].IndexOf(' ') + 1);

                        users.Add(user);
                    }
                }
                return users;
            }
            catch
            {
                throw;
            }
        }

        public void SaveUser(UserInfoModel user)
        {
            if (!File.Exists(_path))
            {
                using (StreamWriter sw = File.CreateText(_path))
                {
                    sw.WriteLine($"Name: {user.name}, Surname: {user.surname}, ID: {user.id}, Dateofbirth: {user.dateOfBirth}");
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(_path))
                {
                    sw.WriteLine($"Name: {user.name}, Surname: {user.surname}, ID: {user.id}, Dateofbirth: { user.dateOfBirth}");
                }
            }
        }

        public void UpdateUser(UserInfoModel newUserInfo, UserInfoModel oldUser)
        {
            var totalString = "";

            try
            {
                using (StreamReader sr = File.OpenText(_path))
                {
                    var s = "";                    
                   
                    while ((s = sr.ReadLine()) != null)
                    {
                        var userInfo = s.Split(',');
                        var newInfo = new List<string>();
                        for (int i = 0; i < userInfo.Length; i++)
                        {
                            var splitString = userInfo[i].Split(':');
                            splitString[1] = splitString[1].Trim();
                            newInfo.Add(splitString[1]);
                        }

                        var matches = checkMatch(newInfo.ToArray(), oldUser);

                        if (matches)
                        {
                            totalString += newUserInfo.ToString();                            
                        }
                        else
                        {
                            totalString += s;
                        }
                        totalString += "\n";
                    }
                }
            }
            catch
            {
                throw;
            }

            try
            {
                using (StreamWriter sw = new StreamWriter(_path, false))
                {
                    sw.Write(totalString);
                }
            }
            catch
            {
                throw;
            }
        }

        public void DeleteUser(UserInfoModel user)
        {
            var totalString = "";

            try
            {
                using (StreamReader sr = File.OpenText(_path))
                {
                    var s = "";

                    while ((s = sr.ReadLine()) != null)
                    {
                        var userInfo = s.Split(',');
                        var newInfo = new List<string>();
                        for (int i = 0; i < userInfo.Length; i++)
                        {
                            var splitString = userInfo[i].Split(':');
                            splitString[1] = splitString[1].Trim();
                            newInfo.Add(splitString[1]);
                        }

                        var matches = checkMatch(newInfo.ToArray(), user);

                        if (matches)
                        {
                            continue;
                        }
                        else
                        {
                            totalString += s;
                        }
                        totalString += "\n";
                    }
                }
            }
            catch
            {
                throw;
            }

            try
            {
                using (StreamWriter sw = new StreamWriter(_path, false))
                {
                    sw.Write(totalString);
                }
            }
            catch
            {
                throw;
            }
        }

        private bool checkMatch(string[] userInfo, UserInfoModel user)
        {
            if (userInfo.Length != 4)
                return false;

            if (user.name != userInfo[0] && user.surname != userInfo[1]
                && user.id != userInfo[2] && user.dateOfBirth != userInfo[3])
                return false;
            return true;
        }
    }
}
