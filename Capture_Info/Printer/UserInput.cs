﻿using Printer;
using System;

namespace Capture_Info.UserIO
{
    public class UserInput : IUserInput
    {
        private readonly IConsole console;

        public UserInput(IConsole console)
        {
            this.console = console;
        }

        public ConsoleKeyInfo GetKey(bool intercept)
        {
            var continueKey = console.ReadKey(intercept);
            return continueKey;
        }

        public string GetInfo(string validationField)
        {
            console.Write(validationField + ": ");

            if (validationField.CompareTo("ID") == 0)
            {
                return EnsureID();
            }
            
            string temp = console.ReadLine();
            return temp;
        }       

        private string EnsureID()
        {
            string _val = "";

            ConsoleKeyInfo key;

            do
            {
                key = console.ReadKey(true);
                if (key.Key != ConsoleKey.Backspace)
                {
                    double val = 0;
                    bool _x = double.TryParse(key.KeyChar.ToString(), out val);
                    if (_x && _val.Length < 13)
                    {
                        _val += key.KeyChar;
                        console.Write(key.KeyChar);
                    }
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && _val.Length > 0)
                    {
                        _val = _val.Substring(0, (_val.Length - 1));
                        console.Write("\b \b");
                    }
                }
            } while (key.Key != ConsoleKey.Enter);
            console.WriteLine();                
            return _val;
        }
    }
}
