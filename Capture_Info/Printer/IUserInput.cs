﻿using System;

namespace Capture_Info.UserIO
{
    public interface IUserInput
    {
        string GetInfo(string s);

        ConsoleKeyInfo GetKey(bool intercept);
    }
}
