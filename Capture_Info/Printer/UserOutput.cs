﻿using System;
using Capture_Info.Repository;
using Printer;

namespace Capture_Info.UserIO
{
    public class UserOutput : IUserOutput
    {
        private readonly IConsole console;

        public UserOutput(IConsole console)
        {
            this.console = console;
        }

        public void PerformOperation(IRepository repository, ConsoleKeyInfo k)
        {
            console.WriteLine();
            if (k.Key == ConsoleKey.Q)
            {
                console.WriteLine("Goodbye");
            }
            else if (k.Key == ConsoleKey.Z)
            {
                console.WriteLine(repository.GetFromPath());
            }
        }

        public void PrintError(string s)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine(s);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Please Try Again.");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
        }

        public void InfoPrompt()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Please provide your Name, Surname, I.D number and DateofBirth when prompted.");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void PrintUsage()
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Enter <z> to print the file...");
            Console.WriteLine("Enter <q> to quit or any other key to continue...");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
