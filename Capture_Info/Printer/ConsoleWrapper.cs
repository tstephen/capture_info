﻿using System;

namespace Printer
{
    public class ConsoleWrapper : IConsole
    {
        public ConsoleKeyInfo ReadKey(bool intercept)
        {
            return Console.ReadKey(intercept);
        }

        public void Write(char value)
        {
            Console.Write(value);
        }

        public void Write(string value)
        {
            Console.Write(value);
        }

        public string ReadLine()
        {
            return Console.ReadLine();
        }

        public void WriteLine(string value = "")
        {
            Console.WriteLine(value);
        }
    }
}
