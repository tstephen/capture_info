﻿using Capture_Info.Repository;
using System;

namespace Capture_Info.UserIO
{
    public interface IUserOutput
    {
        void PerformOperation(IRepository r, ConsoleKeyInfo k);

        void PrintError(string s);

        void InfoPrompt();

        void PrintUsage();
    }
}
