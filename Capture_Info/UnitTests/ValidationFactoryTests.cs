﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Validation;
using Validation.Factories;

namespace UnitTests
{
    // Test that we receive the correct validationObject for every GetInstance
    [TestClass]
    public class ValidationFactoryTests
    {
        [TestMethod]
        public void ReturnsValidValidationObject_forName()
        {
            var systemUnderTest = new FakeValidationFactory();

            var result = systemUnderTest.GetInstance(ValidationField.FirstName);

            Assert.IsInstanceOfType(result, typeof(NameValidation));
        }

        [TestMethod]
        public void ReturnsValidValidationObject_forSurname()
        {
            var systemUnderTest = new FakeValidationFactory();

            var result = systemUnderTest.GetInstance(ValidationField.Surname);

            Assert.IsInstanceOfType(result, typeof(SurnameValidation));
        }

        [TestMethod]
        public void ReturnsValidValidationObject_forID()
        {
            var systemUnderTest = new FakeValidationFactory();

            var result = systemUnderTest.GetInstance(ValidationField.ID);

            Assert.IsInstanceOfType(result, typeof(IdValidation));
        }

		[TestMethod]
		public void ReturnsValidValidationObject_forDateofbirth()
		{
			var systemUnderTest = new FakeValidationFactory();

			var result = systemUnderTest.GetInstance(ValidationField.DateOfBirth);

			Assert.IsInstanceOfType(result, typeof(DateofbirthValidation));
		}

		private class FakeValidationFactory : ValidationFactory
        {  }
    }
}
