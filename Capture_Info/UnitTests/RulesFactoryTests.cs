﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Validation;
using Validation.Factories;

namespace UnitTests
{
    [TestClass]
    public class RulesFactoryTests
    {
        [TestMethod]
        public void ReturnsValidRules_forName()
        {
            var rulesFactory = new RulesFactory();
            var rulesBuilderFactory = new RulesBuilderFactory(rulesFactory);
            var rules = rulesBuilderFactory.GetInstance(ValidationField.FirstName).Create();

            Assert.IsInstanceOfType(rules[0], typeof(MaxLength));
            Assert.IsInstanceOfType(rules[1], typeof(IsNoDigitInString));
            Assert.IsInstanceOfType(rules[2], typeof(IsAlphaName));
        }

        [TestMethod]
        public void ReturnsValidRules_forSurname()
        {
            var rulesFactory = new RulesFactory();
            var rulesBuilderFactory = new RulesBuilderFactory(rulesFactory);
            var rules = rulesBuilderFactory.GetInstance(ValidationField.Surname).Create();

            Assert.IsInstanceOfType(rules[0], typeof(IsNoDigitInString));
            Assert.IsInstanceOfType(rules[1], typeof(IsAlphaSurname));
        }

        [TestMethod]
        public void ReturnsValidRules_forID()
        {
            var rulesFactory = new RulesFactory();
            var rulesBuilderFactory = new RulesBuilderFactory(rulesFactory);
            var rules = rulesBuilderFactory.GetInstance(ValidationField.ID).Create();

            Assert.IsInstanceOfType(rules[0], typeof(OnlyDigitsInString));
            Assert.IsInstanceOfType(rules[1], typeof(IdLength));
            Assert.IsInstanceOfType(rules[2], typeof(IsDateValid));
            Assert.IsInstanceOfType(rules[3], typeof(IsValidResidencyCode));
            Assert.IsInstanceOfType(rules[4], typeof(CheckLuhnDigit));
        }

		[TestMethod]
		public void ReturnsValidRules_forDateOfBirth()
		{
			var rulesFactory = new RulesFactory();
			var rulesBuilderFactory = new RulesBuilderFactory(rulesFactory);
			var rules = rulesBuilderFactory.GetInstance(ValidationField.DateOfBirth).Create();

			Assert.IsInstanceOfType(rules[1], typeof(IsDateValid));
		}
	}
}
