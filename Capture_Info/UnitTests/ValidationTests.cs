﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Validation;

namespace UnitTests
{
	[TestClass]
	public class ValidationTests
	{
		[TestMethod]
		public void Validates_OnMaxLength_WithValidInput()
		{
			//Arrange
			var systemUnderTest = new MaxLength();
			var validName = "TestName";
			//Act
			var result = systemUnderTest.IsValid(validName);
			//Assert
			Assert.IsTrue(result);
		}

		[TestMethod]
		public void Validates_OnIsAlphaName_WithValidInput()
		{
			//Arrange
			var systemUnderTest = new IsAlphaName();
			var validName = "Tyrone";
			//Act
			var result = systemUnderTest.IsValid(validName);
			//Assert
			Assert.IsTrue(result);
		}

        [TestMethod]
        public void Invalidates_OnIsAlphaName_WithEmptyString()
        {
            //Arrange
            var systemUnderTest = new IsAlphaName();
            var invalidName = string.Empty;
            //Act
            var result = systemUnderTest.IsValid(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Invalidates_OnIsAlphaName_WithInvalidCharacters()
        {
            //Arrange
            var systemUnderTest = new IsAlphaName();
            var invalidName = "hell@";
            //Act
            var result = systemUnderTest.IsValid(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_OnIsDigitInString_WithValidInput()
        {
            //Arrange
            var systemUnderTest = new IsNoDigitInString();
            var validName = "Tyrone12";
            //Act
            var result = systemUnderTest.IsValid(validName);
            //Assert
            Assert.IsFalse(result);
        }

        /*[TestMethod]
		public void Invalidates_OnValidateName_WithDigitInString()
		{
			//Arrange
			var systemUnderTest = new UserValidation();
			var invalidName = "Test5";
			//Act
			var result = systemUnderTest.IsValidName(invalidName);
			//Assert
			Assert.IsFalse(result);
		}

		[TestMethod]
		public void Invalidates_OnValidateName_WithNameLongerThan30()
		{
			//Arrange
			var systemUnderTest = new UserValidation();
			var invalidName = "ttttttttttttttttttttttttttttttt";
			//Act
			var result = systemUnderTest.IsValidName(invalidName);
			//Assert
			Assert.IsFalse(result);
		}


        [TestMethod]
        public void Invalidates_OnValidateName_WithNotAlphaName()
        {
            //Arrange
            var systemUnderTest = new UserValidation();
            var invalidName = "Test@";
            //Act
            var result = systemUnderTest.IsValidName(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_OnValidateSurname_WithValidInput()
        {
            var systemUnderTest = new UserValidation();
            var validName = "Stephen";
            var result = systemUnderTest.IsValidSurname(validName);
            Assert.IsTrue(result);
        }

        public void Invalidates_OnValidateSurname_WithEmptyString()
        {
            //Arrange
            var systemUnderTest = new UserValidation();
            var invalidName = "";
            //Act
            var result = systemUnderTest.IsValidSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateSurname_WithDigitInString()
        {
            //Arrange
            var systemUnderTest = new UserValidation();
            var invalidName = "Stephen23";
            //Act
            var result = systemUnderTest.IsValidSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateSurname_WithNotAlpha()
        {
            //Arrange
            var systemUnderTest = new UserValidation();
            var invalidName = "Stephen@";
            //Act
            var result = systemUnderTest.IsValidSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateSurname_WithInvalidCharacters()
        {
            //Arrange
            var systemUnderTest = new UserValidation();
            var invalidName = " -Stephen";
            //Act
            var result = systemUnderTest.IsValidSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_OnValidateIsAlphaSurname_WithValidInput()
        {
            //Arrange
            var validName = "Stephen";
            //Act
            var result = UserValidation.IsAlphaSurname(validName);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateIsAlphaSurname_WithIllegalCharacters()
        {
            //Arrange
            var invalidName = "Step@hen";
            //Act
            var result = UserValidation.IsAlphaSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateIsAlphaSurname_WithConsecutiveSpaces()
        {
            //Arrange
            var invalidName = "  Stephen";
            //Act
            var result = UserValidation.IsAlphaSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateIsAlphaSurname_WithConsecutiveDashes()
        {
            //Arrange
            var invalidName = "Stephen--Lee";
            //Act
            var result = UserValidation.IsAlphaSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateIsAlphaSurname_WithSpaceOnly()
        {
            //Arrange
            var invalidName = " ";
            //Act
            var result = UserValidation.IsAlphaSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateIsAlphaSurname_WithDashOnly()
        {
            //Arrange
            var invalidName = "-";
            //Act
            var result = UserValidation.IsAlphaSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateIsAlphaSurname_WithSpaceFollowingDash()
        {
            //Arrange
            var invalidName = "Stephen- Lee";
            //Act
            var result = UserValidation.IsAlphaSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateIsAlphaSurname_WithDashFollowingSpace()
        {
            //Arrange
            var invalidName = "Stephen -Lee";
            //Act
            var result = UserValidation.IsAlphaSurname(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_OnValidateIsIDValid_WithValidDate()
        {
            //Arrange
            var validId = "9311205185087";
            //Act
            var result = UserValidation.IsIDValid(validId);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateIsIDValid_WithInvalidDate()
        {
            //Arrange
            var invalidID = "9302305185187";
            //Act
            var result = UserValidation.IsIDValid(invalidID);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_OnValidateIsValidResidencyCode_WithValidResidencyCode()
        {
            //Arrange
            var validId = "9311205185087";
            //Act
            var result = UserValidation.IsValidResidencyCode(validId);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Invalidates_OnValidateIsValidResidencyCode_WithInvalidResidencyCode()
        {
            //Arrange
            var validId = "9311205185587";
            //Act
            var result = UserValidation.IsValidResidencyCode(validId);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_OnValidDate_WithValidDate()
        {
            //Arrange
            var validDate = "960728";
            //Act
            var result = UserValidation.IsDateValid(validDate);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Invalidates_OnValidDate_WithInvalidDate()
        {
            //Arrange
            var invalidDate = "960231";
            //Act
            var result = UserValidation.IsDateValid(invalidDate);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_IsDigitInString_WithValidinput()
        {
            //Arrange
            var validString = "T2umelo";
            //Act
            var result = UserValidation.IsDigitInString(validString);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Validates_IsDigitInString_WithInvalidinput()
        {
            //Arrange
            var validString = "Tumelo";
            //Act
            var result = UserValidation.IsDigitInString(validString);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_OnlyDigitsInString_WithValidinput()
        {
            //Arrange
            var validString = "1245678";
            //Act
            var result = UserValidation.OnlyDigitsInString(validString);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Validates_OnlyDigitsInString_WithInvalidinput()
        {
            //Arrange
            var validString = "123k45";
            //Act
            var result = UserValidation.OnlyDigitsInString(validString);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_IsAlphaName_WithValidinput()
        {
            //Arrange
            var validName = "abcdefg";
            //Act
            var result = UserValidation.IsAlphaName(validName);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Validates_IsAlphaName_WithInvalidinput()
        {
            //Arrange
            var invalidName = "abcde5fg";
            //Act
            var result = UserValidation.IsAlphaName(invalidName);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_IsIDValid_WithOnlyDigitsInString()
        {
            //Arrange
            var validId = "9607285136084";
            //Act
            var result = UserValidation.IsIDValid(validId);
            //Assert
            Assert.IsTrue(result);
        }
        //
        [TestMethod]
        public void Validates_IsIDValid_WithNotOnlyDigitsInString()
        {
            //Arrange
            var invalidId = "96072851j6084";
            //Act
            var result = UserValidation.IsIDValid(invalidId);
            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Validates_IsIDValid_WithValidLengthInString()
        {
            //Arrange
            var validIdLength = "9607285136084";
            //Act
            var result = UserValidation.IsIDValid(validIdLength);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Validates_IsIDValid_WithInvalidLengthInString()
        {
            //Arrange
            var invalidId = "96072851446084";
            //Act
            var result = UserValidation.IsIDValid(invalidId);
            //Assert
            Assert.IsFalse(result);
        }

		[TestMethod]
		public void Invalidates_IsAlphaSurname_WithStringMultipleSpaces()
		{
			//Arrange
			var invalidSurname = "Mfene  Tumelo";
			//Act
			var result = UserValidation.IsAlphaSurname(invalidSurname);
			//Assert
			Assert.IsFalse(result);
		}

		[TestMethod]
		public void Invalidates_OnValidateIsAlphaSurname_WithIllegalEndingCharacters()
		{
			//Arrange
			var invalidName = "Stephen-";
			//Act
			var result = UserValidation.IsAlphaSurname(invalidName);
			//Assert
			Assert.IsFalse(result);
		}

		[TestMethod]
		public void Invalidates_IsAlphaSurname_WithStringTab()
		{
			//Arrange
			var invalidSurname = "Mfene	umelo";
			//Act
			var result = UserValidation.IsAlphaSurname(invalidSurname);
			//Assert
			Assert.IsFalse(result);
		}*/
    }
}
