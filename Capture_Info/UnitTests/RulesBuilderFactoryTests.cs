﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Validation;
using Validation.Factories;
using Validation.Rules;

namespace UnitTests
{
    // Test that we receive the correct builder for every GetInstance
    [TestClass]
    public class RulesBuilderFactoryTests
    {       
        [TestMethod]
        public void ReturnsValidBuilder_forName()
        {
            var testFactory = new RulesFactory();
            var systemUnderTest = new FakeRulesBuilderFactory(testFactory);

            var result = systemUnderTest.GetInstance(ValidationField.FirstName);

            Assert.IsInstanceOfType(result, typeof(NameRulesBuilder));
        }

        [TestMethod]
        public void ReturnsValidBuilder_forSurname()
        {
            var testFactory = new RulesFactory();
            var systemUnderTest = new FakeRulesBuilderFactory(testFactory);

            var result = systemUnderTest.GetInstance(ValidationField.Surname);

            Assert.IsInstanceOfType(result, typeof(SurnameRulesBuilder));
        }

        [TestMethod]
        public void ReturnsValidBuilder_forID()
        {
            var testFactory = new RulesFactory();
            var systemUnderTest = new FakeRulesBuilderFactory(testFactory);

            var result = systemUnderTest.GetInstance(ValidationField.ID);

            Assert.IsInstanceOfType(result, typeof(IdRulesBuilder));
        }

		[TestMethod]
		public void ReturnsValidBuilder_forDateofbirth()
		{
			var testFactory = new RulesFactory();
			var systemUnderTest = new FakeRulesBuilderFactory(testFactory);

			var result = systemUnderTest.GetInstance(ValidationField.DateOfBirth);

			Assert.IsInstanceOfType(result, typeof(DateofbirthRulesBuilder));
		}

		private class FakeValidationRulesFactory : IRulesBuilderFactory
        {
            public IRulesBuilder GetInstance(ValidationField field)
            {
                return new FakeEmptyRulesBuilder();
            }

            private class FakeEmptyRulesBuilder : IRulesBuilder
            {
                public List<IValidationRule> Create()
                {
                    return new List<IValidationRule>();
                }
            }
        }

        private class FakeRulesBuilderFactory : RulesBuilderFactory
        {
            public FakeRulesBuilderFactory(IRulesFactory f): base(f)
            {}
        }
    }
}
