﻿using Capture_Info.UserIO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Printer;

namespace UnitTests
{
    [TestClass]
    public class UserInputTests
    {
        private readonly Mock<IConsole> mockConsole;

        public UserInputTests()
        {
            mockConsole = new Mock<IConsole>();
        }

        [TestMethod]
        public void GetsKeyFromUser_onGetKey()
        {
            var systemUnderTest = new UserInput(mockConsole.Object);
            var intercept = true;

            systemUnderTest.GetKey(intercept);

            mockConsole.Verify(mc => mc.ReadKey(intercept), Times.Once);
        }

        [TestMethod]
        public void WritesValueToConsole_onGetInfo()
        {
            var systemUnderTest = new UserInput(mockConsole.Object);
            var testString = "Name";

            systemUnderTest.GetInfo(testString);

            mockConsole.Verify(mc => mc.Write(It.IsAny<string>()), Times.Once);
        }


        [TestMethod]
        public void ReadsLineFromConsole_onGetInfo()
        {
            var systemUnderTest = new UserInput(mockConsole.Object);
            var testString = "Name";

            systemUnderTest.GetInfo(testString);

            mockConsole.Verify(mc => mc.ReadLine(), Times.Once);
        }
    }
}
