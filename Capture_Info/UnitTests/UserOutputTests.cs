﻿using Capture_Info.UserIO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Printer;
using System;
using Capture_Info.Repository;

namespace UnitTests
{
    [TestClass]
    public class UserOutputTests
    {
        private readonly Mock<IConsole> mockConsole;

        public UserOutputTests()
        {
            mockConsole = new Mock<IConsole>();
        }

        [TestMethod]
        public void PrintsGoodbyeIfKeyIsQ_onPerformOperation()
        {
            var systemUnderTest = new UserOutput(mockConsole.Object);
            var key = new ConsoleKeyInfo('Q', ConsoleKey.Q, false, false, false);
            var outputString = "Goodbye";
            var repository = new Mock<UserRepository>();

            systemUnderTest.PerformOperation(repository.Object, key);

            mockConsole.Verify(mc => mc.WriteLine(outputString), Times.Once);
        }

        [TestMethod]
        public void PrintsFileFromPathIfKeyIsZ_onPerformOperation()
        {
            var systemUnderTest = new UserOutput(mockConsole.Object);
            var key = new ConsoleKeyInfo('Z', ConsoleKey.Z, false, false, false);
            var repository = new Mock<UserRepository>();
            var outputText = repository.Object.GetFromPath();

            systemUnderTest.PerformOperation(repository.Object, key);

            mockConsole.Verify(mc => mc.WriteLine(outputText), Times.Once);
        }
    }
}
