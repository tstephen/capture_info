﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Validation;
using Validation.Factories;
using Validation.Rules;

namespace UnitTests
{
	[TestClass]
	public class DateofbirthValidationTests
	{
		[TestMethod]
		public void ReturnsValidResult_forNoRules()
		{
			var testFactory = new FakeValidationRulesFactory();
			var systemUnderTest = new DateofbirthValidation(testFactory);
			var testDate = "96072";

			var result = systemUnderTest.Validate(testDate);

			Assert.IsTrue(result.IsValid);
			Assert.AreEqual("Success", result.ValidationMessage);
		}

		[TestMethod]
		public void ReturnsInvalidResult_forFailingRule()
		{
			var testFactory = new FakeInvalidValidationRulesFactory();
			var systemUnderTest = new DateofbirthValidation(testFactory);
			var testdate = "124578";

			var result = systemUnderTest.Validate(testdate);

			Assert.IsFalse(result.IsValid);
			Assert.AreEqual("Invalid", result.ValidationMessage);
		}

		[TestMethod]
		public void ReturnsValidResult_forPassingRule()
		{
			var testFactory = new FakeValidValidationRulesFactory();
			var systemUnderTest = new DateofbirthValidation(testFactory);
			var testdate = "960728";

			var result = systemUnderTest.Validate(testdate);

			Assert.IsTrue(result.IsValid);
			Assert.AreEqual("Success", result.ValidationMessage);
		}

		private class FakeValidationRulesFactory : IRulesBuilderFactory
		{
			public IRulesBuilder GetInstance(ValidationField field)
			{
				return new FakeEmptyRulesBuilder();
			}

			private class FakeEmptyRulesBuilder : IRulesBuilder
			{
				public List<IValidationRule> Create()
				{
					return new List<IValidationRule>();
				}
			}
		}

		private class FakeInvalidValidationRulesFactory : IRulesBuilderFactory
		{
			public IRulesBuilder GetInstance(ValidationField field)
			{
				return new FakeInvalidRulesBuilder();
			}

			private class FakeInvalidRulesBuilder : IRulesBuilder
			{
				public List<IValidationRule> Create()
				{
					return new List<IValidationRule>() { new FakeInvalidValidationRule() };
				}

				private class FakeInvalidValidationRule : ValidationRule
				{
					public override bool IsValid(string s)
					{
						var isMatch = false;
						if (!isMatch)
						{
							SetInvalidResult("Invalid");
						}
						return Result.IsValid;
					}
				}
			}
		}

		private class FakeValidValidationRulesFactory : IRulesBuilderFactory
		{
			public IRulesBuilder GetInstance(ValidationField field)
			{
				return new FakeValidRulesBuilder();
			}

			private class FakeValidRulesBuilder : IRulesBuilder
			{
				public List<IValidationRule> Create()
				{
					return new List<IValidationRule>() { new FakeValidValidationRule() };
				}

				private class FakeValidValidationRule : ValidationRule
				{
					public override bool IsValid(string s)
					{
						var isMatch = true;
						if (!isMatch)
						{
							SetInvalidResult("Always True!");
						}
						return Result.IsValid;
					}
				}
			}
		}
	}
}
