﻿using Capture_Info;
using Capture_Info.Model;
using Capture_Info.UserIO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Validation;
using Validation.Factories;
using Validation.Validators;

namespace UnitTests
{
    [TestClass]
    public class ManagerTests
    {
        private Mock<IValidationFactory> mockValidationFactory;
        private Mock<IUserInput> mockUserInput;
        private Mock<IUserOutput> mockUserOutput;

        public ManagerTests()
        {
            mockValidationFactory = new Mock<IValidationFactory>();
            mockUserInput = new Mock<IUserInput>();
            mockUserOutput = new Mock<IUserOutput>();
        }

        [TestMethod]
        public void PromptsUserForInput_onGetUserInformation()
        {
            var systemUnderTest = new Manager(mockValidationFactory.Object,
                mockUserInput.Object, mockUserOutput.Object);

            systemUnderTest.PromptUser();

            mockUserOutput.Verify(muo => muo.InfoPrompt());
        }

        [TestMethod]
        public void DoesNotPrintErrorForValidName_onGetValidName()
        {
            var mockValidation = new Mock<IValidation>();
            var trueTestResult = new ValidationResult() { IsValid = true };
            mockValidation.Setup(mv => mv.Validate(It.IsAny<string>())).Returns(trueTestResult);

            mockValidationFactory.Setup(mvf => mvf.GetInstance(ValidationField.FirstName))
                .Returns(mockValidation.Object);

            var systemUnderTest = new Manager(mockValidationFactory.Object,
                mockUserInput.Object, mockUserOutput.Object);
            var testModel = new UserInfoModel() { name = "Test" };

            systemUnderTest.GetValidName(testModel);

            mockUserOutput.Verify(muo => muo.PrintError(It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public void PrintsErrorForInvalidName_onGetValidName()
        {
            //Arrang
            var testName = "testName";
            mockUserInput.Setup(mui => mui.GetInfo("Name")).Returns(testName);//mock capture

            var mockValidation = new Mock<IValidation>();
            var falseTestResult = new ValidationResult() { IsValid = false };
            mockValidation.Setup(mv => mv.Validate(testName)).Returns(falseTestResult);//mock validation

            mockValidationFactory.Setup(mvf => mvf.GetInstance(ValidationField.FirstName))
                .Returns(mockValidation.Object); //mock getInstance

            var systemUnderTest = new Manager(mockValidationFactory.Object,
               mockUserInput.Object, mockUserOutput.Object);
            var testModel = new UserInfoModel();

            mockUserOutput.Setup(muo => muo.PrintError(It.IsAny<string>()))
                .Callback(() => { falseTestResult.IsValid = true; }); //Knock out of the loop

            //Act
            systemUnderTest.GetValidName(testModel);

            //Assert
            mockUserOutput.Verify(muo => muo.PrintError(It.IsAny<string>()), Times.Once);
        }

		//Surname
		[TestMethod]
		public void DoesNotPrintErrorForValidSurname_onGetValidSurname()
		{
			var mockValidation = new Mock<IValidation>();
			var trueTestResult = new ValidationResult() { IsValid = true };
			mockValidation.Setup(mv => mv.Validate(It.IsAny<string>())).Returns(trueTestResult);

			mockValidationFactory.Setup(mvf => mvf.GetInstance(ValidationField.Surname))
				.Returns(mockValidation.Object);

			var systemUnderTest = new Manager(mockValidationFactory.Object,
				mockUserInput.Object, mockUserOutput.Object);
			var testModel = new UserInfoModel() { surname = "Test" };

			systemUnderTest.GetValidSurname(testModel);

			mockUserOutput.Verify(muo => muo.PrintError(It.IsAny<string>()), Times.Never);
		}

		[TestMethod]
		public void PrintsErrorForInvalidSurname_onGetValidSurname()
		{
			//Arrang
			var testSurname = "testSurname";
			mockUserInput.Setup(mui => mui.GetInfo("Surname")).Returns(testSurname);//mock capture

			var mockValidation = new Mock<IValidation>();
			var falseTestResult = new ValidationResult() { IsValid = false };
			mockValidation.Setup(mv => mv.Validate(testSurname)).Returns(falseTestResult);//mock validation

			mockValidationFactory.Setup(mvf => mvf.GetInstance(ValidationField.Surname))
				.Returns(mockValidation.Object); //mock getInstance

			var systemUnderTest = new Manager(mockValidationFactory.Object,
			   mockUserInput.Object, mockUserOutput.Object);
			var testModel = new UserInfoModel();

			mockUserOutput.Setup(muo => muo.PrintError(It.IsAny<string>()))
				.Callback(() => { falseTestResult.IsValid = true; }); //Knock out of the loop

			//Act
			systemUnderTest.GetValidSurname(testModel);

			//Assert
			mockUserOutput.Verify(muo => muo.PrintError(It.IsAny<string>()), Times.Once);
		}

		//ID
		[TestMethod]
		public void DoesNotPrintErrorForValidId_onGetValidId()
		{
			var mockValidation = new Mock<IValidation>();
			var trueTestResult = new ValidationResult() { IsValid = true };
			mockValidation.Setup(mv => mv.Validate(It.IsAny<string>())).Returns(trueTestResult);

			mockValidationFactory.Setup(mvf => mvf.GetInstance(ValidationField.ID))
				.Returns(mockValidation.Object);

			var systemUnderTest = new Manager(mockValidationFactory.Object,
				mockUserInput.Object, mockUserOutput.Object);
			var testModel = new UserInfoModel() { id = "9607285136888" };

			systemUnderTest.GetValidID(testModel);

			mockUserOutput.Verify(muo => muo.PrintError(It.IsAny<string>()), Times.Never);
		}

		[TestMethod]
		public void PrintsErrorForInvalidId_onGetValidId()
		{
			//Arrang
			var testId = "9607285136888";
			mockUserInput.Setup(mui => mui.GetInfo("ID")).Returns(testId);//mock capture

			var mockValidation = new Mock<IValidation>();
			var falseTestResult = new ValidationResult() { IsValid = false };
			mockValidation.Setup(mv => mv.Validate(testId)).Returns(falseTestResult);//mock validation

			mockValidationFactory.Setup(mvf => mvf.GetInstance(ValidationField.ID))
				.Returns(mockValidation.Object); //mock getInstance

			var systemUnderTest = new Manager(mockValidationFactory.Object,
			   mockUserInput.Object, mockUserOutput.Object);
			var testModel = new UserInfoModel();

			mockUserOutput.Setup(muo => muo.PrintError(It.IsAny<string>()))
				.Callback(() => { falseTestResult.IsValid = true; }); //Knock out of the loop

			//Act
			systemUnderTest.GetValidID(testModel);

			//Assert
			mockUserOutput.Verify(muo => muo.PrintError(It.IsAny<string>()), Times.Once);
		}
	}
}
