﻿using System;
using Capture_Info.Model;
using Capture_Info.Repository;
using Capture_Info.UserIO;
using Printer;
using Validation;
using Validation.Factories;

namespace Capture_Info
{
    class Program
    {        
        static void Main(string[] args)
        {                       
            IRepository repo = new UserRepository();
            IValidationFactory factory = new ValidationFactory();
            IConsole console = new ConsoleWrapper();
            IUserInput input = new UserInput(console);
            IUserOutput output = new UserOutput(console);
            IManager manager = new Manager(factory, input, output);

            do
            {
                output.PrintUsage();

                var continueKey = input.GetKey(true);
                output.PerformOperation(repo, continueKey);
                switch (continueKey.Key)
                {
                    case ConsoleKey.Q:
                        return;
                    case ConsoleKey.Z:
                        continue;
                }

                var user = new UserInfoModel();
                manager.GetUserInformation(user);         
                repo.SaveToPath(user);
        
                user = null;
            } while (true);
        }
    }
}


