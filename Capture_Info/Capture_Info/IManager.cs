﻿using Capture_Info.Model;

namespace Capture_Info
{
    public interface IManager
    {
        void GetUserInformation(UserInfoModel user);
    }
}
