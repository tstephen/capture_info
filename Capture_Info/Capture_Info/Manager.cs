﻿using System;
using Capture_Info.Model;
using Capture_Info.UserIO;
using Validation;
using Validation.Factories;

namespace Capture_Info
{
    public class Manager : IManager
    {
        private readonly IValidationFactory factory;
        private readonly IUserInput input;
        private readonly IUserOutput output;

        public Manager(IValidationFactory f, IUserInput i, IUserOutput o)
        {
            factory = f;
            input = i;
            output = o;
        }

        public void GetUserInformation(UserInfoModel user)
        {
            // Capture Info
            PromptUser();

            GetValidName(user);
            GetValidSurname(user);
            GetValidID(user);
			GetValidDateofBirth(user);
		}


		public void PromptUser()
        {
            output.InfoPrompt();
        }

        public void GetValidName(UserInfoModel user)
        {
            ValidationResult result;
            do
            {
                user.name = input.GetInfo("Name");
                result = factory.GetInstance(ValidationField.FirstName).Validate(user.name);
                if (!result.IsValid)
                {
                    output.PrintError(result.ValidationMessage);
                }
            } while (!result.IsValid);
        }

        public void GetValidSurname(UserInfoModel user)
        {
            ValidationResult result;
            do
            {
                user.surname = input.GetInfo("Surname");
                result = factory.GetInstance(ValidationField.Surname).Validate(user.surname);
                if (!result.IsValid)
                {
                    output.PrintError(result.ValidationMessage);
                }
            } while (!result.IsValid);
        }

        public void GetValidID(UserInfoModel user)
        {
            ValidationResult result;
            do
            {
                user.id = input.GetInfo("ID");
                result = factory.GetInstance(ValidationField.ID).Validate(user.id);
                if (!result.IsValid)
                {
                    output.PrintError(result.ValidationMessage);
                }
            } while (!result.IsValid);
        }

		public void GetValidDateofBirth(UserInfoModel user)
		{
			ValidationResult result;
			do
			{
				user.dateOfBirth = input.GetInfo("Date Of Birth");
				result = factory.GetInstance(ValidationField.DateOfBirth).Validate(user.dateOfBirth);

				if (!result.IsValid)
				{
					output.PrintError(result.ValidationMessage);
				}

				/*var ID = user.id.Substring(0, 6);
				if (ID.CompareTo(user.dateOfBirth) != 0)
				{
					result.IsValid = false;
					result.ValidationMessage = "Date entered does not match ID.";
					output.PrintError(result.ValidationMessage);
				}*/

		
			} while (!result.IsValid);
		}

	}
}