﻿using Capture_Info.Model;
using Capture_Info.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Validation;
using Validation.Factories;
using WebAPI_CaptureInfo.Models;

namespace WebAPI_CaptureInfo.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("Api/UserInfo")]
    public class CaptureInfoAPIController : ApiController
    {
        IRepository repo = new UserRepository();
        private readonly IValidationFactory factory = new ValidationFactory();

        [HttpGet]
        [Route("GetInfoByFirstName/{firstName}")]
        public IHttpActionResult GetInfoByFirstName(string firstName)
        {
            var users = repo.GetInfoByFirstName(firstName);

            if (users.Count == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(users);
            }
        }

        [HttpGet]
        [Route("AllInfo")]
        public IHttpActionResult GetAllInfo()
        {
            var users = repo.GetAllInfo();

            if (users.Count == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(users);
            }
        }

        [HttpPost]
        [Route("InsertNewUser")]
        public HttpResponseMessage PostUser(UserInfoModel user)
        {
            var result = Validate(user);
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            var resultJsonString = JsonConvert.SerializeObject(new { user = result.User, success = result.Success, errors = result.Errors }, Formatting.None);
            response.Content = new StringContent(resultJsonString, System.Text.Encoding.UTF8, "text/plain");

            if (result.Success == false)
                return response;             

            try
            {
                repo.SaveUser(user);
            }
            catch
            {
                throw;
            }
            
            return response;
        }

        [HttpPost]
        [Route("UpdateUser")]
        public HttpResponseMessage PutUserInfo([FromBody] UpdateUserModel updateInfo)
        {
            var result = Validate(updateInfo.newUserInfo);
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            var resultJsonString = JsonConvert.SerializeObject(new { user = result.User, success = result.Success, errors = result.Errors }, Formatting.None);
            response.Content = new StringContent(resultJsonString, System.Text.Encoding.UTF8, "text/plain");

            if (result.Success == false)
                return response;

            try
            {
                repo.UpdateUser(updateInfo.newUserInfo, updateInfo.oldUser);
            }
            catch
            {
                throw;
            }
            return response;
        }

        [HttpPost]
        [Route("DeleteUser")]
        public IHttpActionResult DeleteUser(UserInfoModel user)
        {
            // TODO: Validate user first
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                repo.DeleteUser(user);
            }
            catch
            {
                throw;
            }
            return Ok(user);
        }

        private ResponseModel Validate(UserInfoModel user)
        {
            var response = new ResponseModel();
            var errors = new Dictionary<string, string>();
            ValidationResult result;
            
            result = factory.GetInstance(ValidationField.FirstName).Validate(user.name);
            if (!result.IsValid)
            {
                errors.Add("FirstName", result.ValidationMessage);
            }

            result = factory.GetInstance(ValidationField.Surname).Validate(user.surname);
            if (!result.IsValid)
            {
                errors.Add("Surname", result.ValidationMessage);
            }

            result = factory.GetInstance(ValidationField.ID).Validate(user.id);
            if (!result.IsValid)
            {
                errors.Add("ID", result.ValidationMessage);
            }

            result = factory.GetInstance(ValidationField.DateOfBirth).Validate(user.dateOfBirth);
            if (!result.IsValid)
            {
                errors.Add("DateOfBirth", result.ValidationMessage);
            }

            response.Success = errors.Count == 0 ? true : false;
            response.User = user;
            response.Errors = errors;

            return response;
        }
    }
}
 