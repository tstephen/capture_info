﻿using Capture_Info.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI_CaptureInfo.Models
{
    public class ResponseModel
    {
        public UserInfoModel User;
        public bool Success;
        public IDictionary<string, string> Errors;
    }
}